package buu.informatics.s59160977.myapplication

import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity

class VocabularyActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var listView = findViewById(R.id.listView) as ListView
        var arrVocabulary: ArrayList<Vocabulary> = ArrayList()
        arrVocabulary.add(Vocabulary("BEAR", R.drawable.bear))

        listView.adapter = CustomAdapter(applicationContext,arrVocabulary)
    }
}