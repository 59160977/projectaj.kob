package buu.informatics.s59160977.myapplication


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.setContentView
import buu.informatics.s59160977.myapplication.databinding.ActivityMainBinding
import buu.informatics.s59160977.myapplication.databinding.FragmentHomepageBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 *
 */
class VocabularyFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding =
            DataBindingUtil.inflate<buu.informatics.s59160977.myapplication.databinding.FragmentVocabularyBinding>(
                inflater,
                R.layout.fragment_vocabulary,
                container,
                false
            )

        var arrVocabulary: ArrayList<Vocabulary> = ArrayList()
        arrVocabulary.add(Vocabulary("BEAR  (แบร์)  หมี", R.drawable.bear))
        arrVocabulary.add(Vocabulary("BIRD  (เบิร์ด)  นก", R.drawable.bird))
        arrVocabulary.add(Vocabulary("CAT   (แค็ท)  แมว", R.drawable.cat))
        arrVocabulary.add(Vocabulary("COBRA (โค-บระ) งูเห่า", R.drawable.cobra))
        arrVocabulary.add(Vocabulary("EAGLE (อิ-เกิล) นกอินทรี", R.drawable.eagle))
        arrVocabulary.add(Vocabulary("GOOSE (กูส)  ห่าน", R.drawable.goose))
        arrVocabulary.add(Vocabulary("KOALA (โคอาลา) หมีโคอาลา", R.drawable.koala))
        arrVocabulary.add(Vocabulary("PENGUIN (แพน-ควิน) เพนกวิน", R.drawable.penguin))
        arrVocabulary.add(Vocabulary("RACCOON (เรคูน) แร็คคูน", R.drawable.raccoon))


        binding.listView.adapter = this.context?.let { CustomAdapter(it,arrVocabulary) }
        return binding.root

    }
}
