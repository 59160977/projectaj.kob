package buu.informatics.s59160977.myapplication


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.DataBindingUtil.inflate
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import kotlinx.android.synthetic.main.fragment_homepage.*
import kotlinx.android.synthetic.main.fragment_homepage.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class HomepageFragment : Fragment() {
    internal lateinit var handler: Handler
    internal lateinit var runnable: Runnable

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        val binding = inflate<buu.informatics.s59160977.myapplication.databinding.FragmentHomepageBinding>(inflater,R.layout.fragment_homepage,container,false)
        // Inflate the layout for this fragment
        binding.startButton.setOnClickListener{ view ->
            view.findNavController().navigate(R.id.action_homepageFragment_to_vocabularyFragment)

            var alert:Int = Toast.LENGTH_SHORT;
            var toast = Toast.makeText(context,"Wellcome to All Animals Vocabulary",alert)

            toast.show()
            handler = Handler()
            runnable = Runnable() {
                handler.postDelayed(runnable, 200)
            }
        }

        return binding.root
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.option_menu, menu)
        if (null == getShareIntent().resolveActivity(activity!!.packageManager)) {
            // hide the menu item if it doesn't resolve
            menu?.findItem(R.id.shareFragment)?.setVisible(false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item!!.itemId) {
            R.id.shareFragment -> shareSuccess()
        }
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }

    private fun getShareIntent() : Intent {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT,"Share")
        return shareIntent
    }

    private fun shareSuccess() {
        startActivity(getShareIntent())
    }

}
